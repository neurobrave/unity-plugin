
*****getting started:

to start working, NeuroSpeed cloud credentials must be entered in a file 

\unity-plugin\Important for unity\Resources\Json\config.txt

following credentials are nesessary: 

  "accountId": string, found on the Profile page of the neurospeed.io dashboard
  "username": string, username for user-level acceess. NOT the same username used to log into the dashboard.
  "password": string, password for the username.

to select which neuromarker is used by default, use the field

"metric": ["my_marker_name", "soft_decision"]

consult decomentation or contact support@neurobrave.com for full list of available markers.


in the NeurosoeedDLL folder there is the code that implements connectivity to neurospeed.io servers and reading the data stream
the folder "important for unity" has the required DLLs, mandatory to make the neurospeed API work. 
neurospeed.DLL.dll has the neurospeed API implementation inside it.

if neurospeed API code is altered, it's nesessary to rebuild the solution and replace the old neurospeed DLL with new one.

Example usage script: /important for unity/NeurospeedAPI.cs
	the script file "UnityThread.cs" is responsilble for events to run in main thread nad this is required for the API to work  in Unity.

folder "Resources" contains the config file for assess credentials and the neuromarker selection. this folder must be copied as is into the "Assets" folder. it's forbidden to rename the "Resources" folder; it must be named "Resources"




 
Running the scripts:

to run the example, attach the script "NeurospeedAPI" to an object in the Unity scene; then it's possible to run the program and see the prints. 


Known issues:

in older Unity versions there was a bug preventing build because of one of the DLL's (probably something to do with socket.io version).
