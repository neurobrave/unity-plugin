using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using NeurospeedDLL;

public class NeurospeedAPI : MonoBehaviour
{
    Neurospeed neurospeed;
    Action rot;

    // Start is called before the first frame update
    void Start()
    {
        neurospeed = new Neurospeed();
        neurospeed.Init();
        Neurospeed.Subscribe(MyMetricEventHandler,
            (data => data.GetMetricData(neurospeed.GetMetric()) < 1.9), 3, "Satisfied!");
        rot = CreateCubeAndChangeSize;
    }

    //the function that runs when the constraint is satisfied.
    public void MyMetricEventHandler(object sender, MetricArgs data)
    {
        UnityThread.executeInUpdate(rot);
    }

    public void CreateCubeAndChangeSize()
    {
        GameObject.CreatePrimitive(PrimitiveType.Cube);
        transform.localScale = new Vector3(3, 1, 5);
    }

    void OnDisable()
    {
        neurospeed.Dispose();
    }

    void Awake()
    {
        UnityThread.InitUnityThread();
    }
}
