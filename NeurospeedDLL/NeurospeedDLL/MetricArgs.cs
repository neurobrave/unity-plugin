﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace NeurospeedDLL
{
    public class MetricArgs
    {
        public JObject output;

        public MetricArgs(JObject jsonData)
        {
            output = jsonData;
        }

        //given a specific metric the function gets the float of that metric in the json data.
        public float GetMetricData(string[] metrics)
        {
            Debug.Log(output);
            foreach (KeyValuePair<string, JToken> pair in output)
            {
                if (pair.Key == metrics[0])
                {
                    float metricValue;
                    if (metrics.Length == 1)
                    {
                        metricValue = (float)pair.Value;
                        Debug.Log(metricValue);
                        return metricValue;
                    }
                    metricValue = (float)pair.Value[metrics[1]];
                    Debug.Log(metricValue);
                    return metricValue;
                }
            }
            return 0;
        }
    }
}
