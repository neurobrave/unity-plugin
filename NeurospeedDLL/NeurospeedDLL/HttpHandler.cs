﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace NeurospeedDLL
{
    internal class HttpHandler : MonoBehaviour
    {
        string token;

        internal string GetHttp(HttpClient client, string uri)
        {
            var endpoint = new Uri(uri);
            var response = client.GetAsync(endpoint).Result;
            return response.Content.ReadAsStringAsync().Result;
        }

        internal void PostHttp(HttpClient client, string uri, string accId, string username, string password)
        {
            var login = new Uri(uri);
            var values = new
            { account_id = accId, username = username, password = password };
            string jsonData = JsonConvert.SerializeObject(values);
            var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            var response = client.PostAsync(login, content).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            dynamic data = JObject.Parse(result);
            token = data.token.accessToken;
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);

            Debug.Log("Post http finished!");
        }

        internal string GetToken()
        {
            return token;
        }
    }
}
