﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace NeurospeedDLL
{
    internal class Processor
    {
        //event for each metric.
        internal event EventHandler<MetricArgs> MetricAnalysys;

        //list of subscribed metrics.
        private List<SubscriptionData<MetricArgs>> subscriptions =
                                new List<SubscriptionData<MetricArgs>>();

        internal void FireEvents(JObject jsonData)
        {
            //call invoke for all the events (will run if subscribed)
            MetricAnalysys?.Invoke(this, new MetricArgs(jsonData));
        }

        internal void Subscribe(EventHandler<MetricArgs> metricDataHandler,
                       Predicate<MetricArgs> constraint, int points,
                       string customName)
        {
            SubscriptionData<MetricArgs> subscriptionData =
                new SubscriptionData<MetricArgs>(metricDataHandler,
                                                            constraint, points, customName);
            subscriptions.Add(subscriptionData);
            MetricAnalysys += subscriptionData.CallSubscriber;
        }

        //use overload to write unsubscribe method for all the events.
        internal void UnSubscribe(EventHandler<MetricArgs> metricDataHandler)
        {
            SubscriptionData<MetricArgs> subscriptionData =
                subscriptions.Find(data => data.eventDataHandler == metricDataHandler);
            if (subscriptionData != null)
            {
                MetricAnalysys -= subscriptionData.CallSubscriber;
            }
        }
    }
}
