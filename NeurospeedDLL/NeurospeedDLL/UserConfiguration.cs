﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NeurospeedDLL
{
    [Serializable]
    public class UserConfiguration
    {
        public string httpUri;
        public string accountId;
        public string username;
        public string password;
        public string socketUrl;
        public string socketPath;
        public string[] metric;
    }

    public class JsonUtils
    {
        public static T ImportJson<T>(string path)
        {
            TextAsset textAsset = Resources.Load<TextAsset>(path);
            return JsonUtility.FromJson<T>(textAsset.text);
        }
    }

}
