﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIOClient;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace NeurospeedDLL
{
    public class Neurospeed : MonoBehaviour
    {
        private HttpHandler httpHandler;
        private SocketIOHandler socketHandler;
        private HttpClient httpClient;
        private SocketIO socketioClient;
        private UserConfiguration userConfiguration;
        internal static Processor processor = new Processor();
        private string[] metric;

        public void Init()
        {
            httpHandler = new HttpHandler();
            httpClient = new HttpClient();
            socketHandler = new SocketIOHandler();
            //find a way to read external json file.
            userConfiguration = JsonUtils.ImportJson<UserConfiguration>("Json/config");
            metric = userConfiguration.metric;
            //Application.dataPath + "/config.txt"
            //string configFile = File.ReadAllText(Application.dataPath + "/config.txt");

            httpHandler.PostHttp(httpClient, userConfiguration.httpUri,
                    userConfiguration.accountId, userConfiguration.username, userConfiguration.password);

            socketioClient =
                    socketHandler.CreateClient(userConfiguration.socketUrl,
                    httpHandler.GetToken(), userConfiguration.socketPath);

            socketHandler.Config(socketioClient);
            socketHandler.Connect(socketioClient);
        }

        public void Dispose()
        {
            httpClient.Dispose();
            socketHandler.Disconnect(socketioClient);
        }

        public static void Subscribe(EventHandler<MetricArgs> metricDataHandler,
                Predicate<MetricArgs> constraint, int points,
                string customName = "Test Event")
        {
            processor?.Subscribe(metricDataHandler, constraint, points, customName);
        }

        public static void UnSubscribe(EventHandler<MetricArgs> metricDataHandler)
        {
            processor?.UnSubscribe(metricDataHandler);
        }

        public string[] GetMetric()
        {
            return metric;
        }
    }
}
