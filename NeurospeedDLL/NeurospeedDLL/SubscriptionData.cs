﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace NeurospeedDLL
{
    internal class SubscriptionData<T>
    {
        internal EventHandler<T> eventDataHandler;
        internal Predicate<T> constraint;
        internal int points;
        internal string customName;

        private int matchCounter;

        internal SubscriptionData()
        {
        }

        internal SubscriptionData(EventHandler<T> eventDataHandler,
                                  Predicate<T> constraint,
                                  int points, string customName)
        {
            this.eventDataHandler = eventDataHandler;
            this.constraint = constraint;
            this.points = points;
            this.customName = customName;
        }

        //checks the constraint of the subscribed event and calls the function given by the developer if satisfied.
        internal void CallSubscriber(object sender, T data)
        {
            bool isMatch = true;
            if (constraint != null)
            {
                isMatch = constraint(data);
                if (isMatch)
                {
                    matchCounter++;
                }
                else
                {
                    matchCounter = 0;
                }
                isMatch = (matchCounter == points);
            }
            if (isMatch)
            {
                matchCounter = 0;
                Debug.Log("Call subscriber " + customName);
                eventDataHandler(sender, data);
            }
        }
    }
}
