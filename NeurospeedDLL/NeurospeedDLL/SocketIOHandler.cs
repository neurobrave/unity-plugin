﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIOClient;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NeurospeedDLL
{
    internal class SocketIOHandler : MonoBehaviour
    {
        internal SocketIO CreateClient(string url, string token, string path)
        {
            return new SocketIO(url, new SocketIOOptions
            {
                Path = path,
                Transport = SocketIOClient.Transport.TransportProtocol.WebSocket,
                Query = new Dictionary<string, string>
                {
                    {"jwt_token", token }
                },
            });
        }
        internal void Config(SocketIO client)
        {
            client.OnConnected += Socket_OnConnected;

            client.OnDisconnected += Socket_OnDisconnected;

            client.On("data", response =>
            {
                ParseResponse(response);
            });
        }
        internal void Connect(SocketIO client)
        {
            client.ConnectAsync();
        }

        internal void Disconnect(SocketIO client)
        {
            client.DisconnectAsync();
        }

        internal static void Socket_OnConnected(object sender, EventArgs e)
        {
            Debug.Log("Socket_OnConnected");
        }

        internal static void Socket_OnDisconnected(object sender, string e)
        {
            Debug.Log("disconnect: " + e);
        }

        //get the response and send a Json of the "output" section in the data
        internal void ParseResponse(SocketIOResponse socketResponse)
        {
            string response = socketResponse.ToString();
            string responseRaw = response.Substring(1, response.Length - 2);
            JObject responseJson = JObject.Parse(responseRaw);
            foreach (KeyValuePair<string, JToken> pair in responseJson)
            {
                if (pair.Key == "output")
                {
                    responseJson = (JObject)pair.Value;
                }
            }
            Neurospeed.processor.FireEvents(responseJson);
        }
    }
}

